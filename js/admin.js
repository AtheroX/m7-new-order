google.charts.load('current', {'packages':['corechart']});
google.charts.setOnLoadCallback(drawChart);

if(sessionStorage.getItem('communities') == null || sessionStorage.getItem('communities') == 'null')
    sessionStorage.setItem('communities', 'Nahant:11;Concord:52;Lexington:112;Quincy:128;Apendi City:98;Salem:37');

var communities = sessionStorage.getItem('communities');
var communitiesArray = communities.split(";");

function drawChart() {

    var textComm = [];
    communitiesArray.forEach(element => {
        var a = element.split(":");
        var c = a[0];
        var n = parseInt(a[1]);
        textComm.push([c,n]);
    });

    console.log(textComm);

    var data = new google.visualization.DataTable();
    data.addColumn("string","Comunidades");
    data.addColumn("number","Dinero × Comunidad (en millones)");
    data.addRows(textComm);
    

    var options = {
        colors: [ '#0abdc6', '#ea00d9', '#ff0000', '#32ec32', '#133e7c', '#3e0752'],
        title: 'Cantidad de dinero (en millones) por distrito',
        is3D:true,
        fontName: "pp",
        pieSliceText: 'value',
        fontSize: 20,
        backgroundColor: 'none',
        legend: {
            textStyle: {
                color: 'white'                
            },
            position: 'bottom'
        },
        titleTextStyle: {
            color: 'white'
        }
    };

    var chart = new google.visualization.PieChart(document.getElementById('piechart'));

    chart.draw(data, options);
}

var eventRaised = false;
var pulsado = false;
var eventos = [
    'Tormenta:🌩:yellow:Ha ocurrido una tormeta que ha destruido una parte de ',
    'Asalto:⚔:red:Ha ocurrido un asalto a ',
    'Tornado:🌪:gray:Un tornado ha arrasado con una parte de la ciudad de ',
    'Hackeo:👨‍💻:green:Un equipo de hackers han atacado la ciudad '
];
var comunidadMuerta = "Comunidad muerta:☠:white:Ha muerto la comunidad {0} por falta de dinero";

var haMuerto = [false,""];
function evento(moricion = false){
    if(eventRaised || pulsado)
        return;
    
    pulsado = true;
    console.log("pulsa");

    var basecolor = $('#eventRaise').css('background-color');
    console.log(basecolor);
    $('#eventRaise').css('background-color','rgb(153,19,19)');

    
    // $('#alerta').css('display','grid');
    $('#alerta').fadeIn(500);
    setTimeout(()=>{
        eventRaised = true;
        pulsado = false;
        $('#eventRaise').css('background-color',basecolor);
        console.log("canRepulsar");

    },500);
    
    var randomEvent = Math.floor(Math.random() * eventos.length);
    var evento = eventos[randomEvent].split(":");
    if(moricion) evento = comunidadMuerta.split(":");
    
    if(!moricion){
        var a = [];
        communitiesArray.forEach(e => {
            if(parseInt(e.split(":")[1]) != 0)
                a.push(e);
        });

        var randomCity = Math.floor(Math.random() * a.length);
        var city = a[randomCity].split(":");

        var money = Math.floor(Math.random() * city[1]/3+1);
        if(city[1] - money <=0){
            haMuerto[0] = true;
            haMuerto[1] = city[0];
            console.log("LA city "+city[0]+" ha murido");
        }
    }else{
        haMuerto[0] = false;
    }

    $('#title').text(evento[0]);
    if(evento[3].includes("{0}")){
        var descs = evento[3].split("{0}");
        $('#desc').text(descs[0]+haMuerto[1]+descs[1]);
    }else{
        $('#desc').text(evento[3]+city[0]);
    }
    if(!moricion) $('#money').text("Ha perdido "+money+" millones");
    $('#emoji').text(evento[1]).css('color',evento[2]);

    if(!moricion){
        var index = (communitiesArray.indexOf(city[0]+":"+city[1]));
        communitiesArray[index] = city[0]+":"+(parseInt(city[1])-money);

        sessionStorage.setItem('communities', communitiesArray.join(";"));
        drawChart();
    }

    var eventosPasados = sessionStorage.getItem('webticker');
    if(eventosPasados == null || eventosPasados == 'null'){
        eventosPasados = [
            "La ciudad Nahant fue atacada por un grupo de radicales de Apendi City",
            "New Order ha instaurado un nuevo método de compra intercambiando órganos",
            "La nueva \"Utopia\" instaurada por New Order se llamará Salem y empieza con un capital de 37 millones recaudados por las diferentes ciudades"
        ];
    }else{
        eventosPasados = eventosPasados.split(";");
    }
    if(moricion) {
        eventosPasados.unshift(descs[0]+haMuerto[1]+descs[1]);
    }else {
        eventosPasados.unshift(evento[3]+city[0] + " perdiendo "+money+ (money==1?" millón":" millones"));
    }
    if(eventosPasados.length>5){
        eventosPasados.splice(6,eventosPasados.length-6);
    }
    sessionStorage.setItem('webticker', eventosPasados.join(";"))

}

function oke(){
    if(!eventRaised)
        return;

    $('#alerta').fadeOut(500);
    setTimeout(()=>{
        // $('#alerta').css('display','none');
        eventRaised = false;
        if(haMuerto[0]){
            evento(true);
            haMuerto[0] = false;
            console.log("Evento de moricion");
        }
    },500);
    
}

$(document).ready(function () {
    $('#alerta').css('display','grid');

    $('#alerta').fadeOut(10);

    var a = JSON.parse(sessionStorage.getItem('user'));
    if(!a['admin']){
        window.location.href = "../";
    }
    
});

$(document).resize(function(){
    drawChart();
});