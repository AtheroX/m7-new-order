$(document).ready(function () {

    $('#hide').fadeOut(1);
    var canFadeIn = true;

    $('#copy').on( 'mouseenter',
        function(){
            if(canFadeIn){
                $(this).fadeOut(100);
                $('#hide').fadeIn(200);
                canFadeIn =false;
            }
        }
    );

    $('#hide').on('mouseleave', function(){
        $(this).fadeOut(100);
        $('#copy').fadeIn(200);
        setTimeout(()=>{canFadeIn = true;},200);
    });
});