

const type = {"suministros":"suministros", "defensas":"defensas", "trabajos":"trabajos", "ocio":"ocio"};
function getTypeImg(tipo){
    switch (tipo) {
        case "suministros":
            return "../img/icons/iconoSuministros.png";
    
        case "defensas":
            return "../img/icons/iconoDefensas.png";

        case "trabajos":
            return "../img/icons/iconoTrabajo.png";
    
        case "ocio":
            return "../img/icons/iconoOcio.png";
    
        default:
            return "";
    }
}

function autoLorem(size){
    var words =["The sky", "above", "the port","was", "the color of television", "tuned", "to", "a dead channel", "All", "this happened", "more or less" ,"I", "had", "the story", "bit by bit", "from various people", "and", "as generally", "happens", "in such cases", "each time", "it", "was", "a different story" ,"It", "was", "a pleasure", "to", "burn"];
    var text = [];
    var x = size;
    while(--x) text.push(words[Math.floor(Math.random() * words.length)]);

    return text.join(" ");
}

function Item(title, duration, price, desc, properties, onlyone, salario=""){
    this.title = title;
    this.duration = duration;
    this.price = price;
    this.img = getTypeImg(properties);
    this.desc = desc;
    this.properties = properties;
    this.onlyone = onlyone;

    var tienda = document.getElementById("tiendas");

    var divo = document.createElement("div");
    divo.className = "item "+properties+ (onlyone?" onlyone":"");
    tienda.appendChild(divo);

    var titulo = document.createElement("h2");
    titulo.innerHTML = title+(duration!=""?"<br>"+duration:"");
    divo.appendChild(titulo);
    
    if(price !== ""){
        var presio = document.createElement("p");
        presio.className = "price";
        presio.innerText = "Price: "+price;
        divo.appendChild(presio);
    }

    var image = document.createElement("img");
    console.log(img);
    image.src = img;
    divo.appendChild(image);

    var descs = document.createElement("div");
    descs.className="desc";
    divo.appendChild(descs);

    desc.forEach(e => {
        var p = document.createElement("p");
        p.innerHTML = (e=="Lorem"?autoLorem(20):e);
        descs.appendChild(p);
    });
    if(salario!=""){
        var p = document.createElement("p");
        p.className = "salario";
        p.innerHTML = "<br>Salario: "+salario+"/mes";
        descs.appendChild(p);
    }

    divo.innerHTML += '<div class="buy"><div class="less"><cite><p>-</p></cite></div><cite class="val">0</cite><div class="add"><cite><p>+</p></cite></div></div>';

}

$(document).ready(function () {

    var tienda = $('#tiendas');
    var cesta = $('#cesta');
    var cestaItems = $('#cestaItems');
    var cestaItemsPrice = $('.wide');
    var cestaPrecio = 0;


    var myMoney = $('#myMoney').find('h1');
    var user = JSON.parse(sessionStorage.getItem("user"))['username'];
    updateMoney();

    var defaultSuministros = ["Pack de suministros de un mes para dos personas a un precio de 20, en los cuales puedes encontrar víveres, medicinas y cosas variadas."];
    var defaultGuardaspaldas = ["Contrato de guardaspaldas puedes escoger un servicio de 2h/4h/8h por el precio de 80/150/250 dineros , gozaras de la mejor seguridad de <cite>NEW ORDER</cite>."];
    var defaultTrabajos = ["Puedes obtar a obtener un trabajo por el cual obtendras un salario x/mes."];
    var defaultOrdenador = ["Puedes comprar un ordenador de ultima generacion para hacer tus gestiones o compras en <cite>NEW ORDER</cite> tus sumunistros, guardaspalda o obtener trabajo."];

    Item(title="Suministros de un mes",duration="(2 pers.)",price="20",desc=defaultSuministros,properties=type.suministros, onlyone=false);
    Item(title="Suministros de un mes",duration="(4 pers.)",price="35",desc=defaultSuministros,properties=type.suministros, onlyone=false);
    Item(title="Guardaspaldas",duration="(2 horas)",price="80",desc=defaultGuardaspaldas,properties=type.defensas, onlyone=false);
    Item(title="Guardaspaldas",duration="(4 horas)",price="150",desc=defaultGuardaspaldas,properties=type.defensas, onlyone=false);
    Item(title="Guardaspaldas",duration="(8 horas)",price="250",desc=defaultGuardaspaldas,properties=type.defensas, onlyone=false);
    Item(title="Trabajo en construcción de rascacielos",duration="",price="",desc=defaultTrabajos,properties=type.trabajos, onlyone=true, salario="550");
    Item(title="Trabajo en construcción de rascacielos",duration="",price="",desc=defaultTrabajos,properties=type.trabajos, onlyone=true, salario="1500");
    Item(title="Ordenador de última generación",duration="",price="500",desc=defaultOrdenador,properties=type.ocio, onlyone=false);

    $('#btnSuministros').bind('mouseup', function(event){

        $('.suministros').removeClass('undisplay');
        $('.defensas')   .addClass   ('undisplay');
        $('.ocio')       .addClass   ('undisplay');
        $('.trabajos')   .addClass   ('undisplay');

    });

    $('#btnTrabajos').bind('mouseup', function(event){

        $('.suministros').addClass   ('undisplay');
        $('.defensas')   .addClass   ('undisplay');
        $('.ocio')       .addClass   ('undisplay');
        $('.trabajos')   .removeClass('undisplay');

    });

    $('#btnOcio').bind('mouseup', function(event){

        $('.suministros').addClass   ('undisplay');
        $('.defensas')   .addClass   ('undisplay');
        $('.ocio')       .removeClass('undisplay');
        $('.trabajos')   .addClass   ('undisplay');

    });

    $('#btnDefensas').bind('mouseup', function(event){

        $('.suministros').addClass   ('undisplay');
        $('.defensas')   .removeClass('undisplay');
        $('.ocio')       .addClass   ('undisplay');
        $('.trabajos')   .addClass   ('undisplay');

    });

    $('#btnTodos').bind('mouseup', function(event){

        $('.suministros').removeClass('undisplay');
        $('.defensas')   .removeClass('undisplay');
        $('.ocio')       .removeClass('undisplay');
        $('.trabajos')   .removeClass('undisplay');

    });

    $('.add').bind('mouseup', function(event){
        var div = $(event.target).parent().parent().parent();
        var item = $(div).parent();

        var val = div.children(1)[1];

        if(parseInt($(val).text()) == 0){
            var addToCestita = $('<button class="addCesta"><cite>ANADIR A LA CESTA</cite></button>');
            div.parent().append(addToCestita);

            $(item).find("button").bind("mouseup", function(ev){
                for (let i = 0; i < parseInt($(val).text()); i++) {
                         
                    var cestaItem = $('<div class="cestaItem"></div>');
                    cestaItem.insertBefore(cestaItemsPrice);

                    console.log(cestaItem);

                    var cestaItemName = $('<p>'+item.find("h2").text()+'</p>');
                    cestaItem.append(cestaItemName);

                    var cestaItemImg = $('<img src="'+item.find("img").attr("src")+'">');
                    cestaItem.append(cestaItemImg);

                    if(item.hasClass("trabajos")){
                        var cestaItemPrice = $('<p class="salario">Salario: '+item.find("p.salario").text().substring(9)+'</p>');
                        cestaItem.append(cestaItemPrice);

                    }else{
                        var cestaItemPrice = $('<p class="salariont">Precio: '+item.find("p.price").text().substring(7)+'</p>');
                        cestaItem.append(cestaItemPrice);

                        //SumaPrecio
                        var coso = (item.find("p.price")==NaN?"0":item.find("p.price").text().substring(7));
                        console.log(coso);
                        cestaPrecio += parseInt(coso);
                        cestaItemsPrice.find('#cestaPrice').text("Precio total: " + cestaPrecio);
                    }
                }
                $(item).find("button").remove();
                $(val).text(0);

                var dinerosActuales = parseInt(sessionStorage.getItem(user));
                if(cestaPrecio > dinerosActuales){
                    alert("Te has pasado, no puedes comprar todo eso");
                    var len = cestaItems.children().length;
                    cestaItems.children().each(function(i,e) {
                        if(i<2 || i == len-1)
                            return;
                        cestaItems.children()[2].remove();
                    });
            
                    cestaPrecio = 0;
                    cestaItemsPrice.find('#cestaPrice').text("Precio total: " + cestaPrecio);
            
                }
            });
        }

        if($(item).hasClass('onlyone')){
            if(parseInt($(val).text()) < 1){
                $(val).text(parseInt($(val).text())+1);
            }
        }else{
            $(val).text(parseInt($(val).text())+1);
        }
    });

    $('.less').bind('mouseup', function(event){
        var div = $(event.target).parent().parent().parent();
        var item = $(div).parent();

        var val = div.children(1)[1];
        if(parseInt($(val).text()) == 1){
            $(item).find("button").remove();
        }
        
        if(parseInt($(val).text()) > 0){
            $(val).text(parseInt($(val).text())-1);
        }
    });

    $('#hideItems').bind('mouseup',function(event){
        var itemsDisplay = cestaItems.css("display");
        console.log(itemsDisplay);
        if(itemsDisplay == "flex"){
            cestaItems.css("display", "None");
        }else{
            cestaItems.css("display", "flex");
        }
    });

    $('#pagar').bind('mouseup', function(ev){
        cestaItems.children().each(function(i,e) {
            if(i<2 || i == cestaItems.children().length-1)
                return;

            var salariont = $($(e).children()[2]);
            var dinerosActuales = parseInt(sessionStorage.getItem(user));
            var precioDeObjeto = parseInt(salariont.text().substring(8));
            var salariodeObjeto = salariont.text().substring(9)
            salariodeObjeto = parseInt(salariodeObjeto.substring(0,salariodeObjeto.length-4));
            console.log(salariodeObjeto)
            console.log(precioDeObjeto);
            if(salariont.hasClass('salariont')){
                sessionStorage.setItem(user, dinerosActuales-precioDeObjeto);
            }else{
                sessionStorage.setItem(user, dinerosActuales+salariodeObjeto); 
            }
            // cestaItems.children()[2].remove();
        });

        var len = cestaItems.children().length;
        cestaItems.children().each(function(i,e) {
            if(i<2 || i == len-1)
                return;
            cestaItems.children()[2].remove();
        });

        cestaPrecio = 0;
        cestaItemsPrice.find('#cestaPrice').text("Precio total: " + cestaPrecio);

        var itemsDisplay = cestaItems.css("display");
        console.log(itemsDisplay);
        if(itemsDisplay == "flex"){
            cestaItems.css("display", "None");
        }else{
            cestaItems.css("display", "flex");
        }


        updateMoney();
        alert("Compra terminada");
    });

});

function updateMoney(){
    var myMoney = $('#myMoney').find('h1');
    var user = JSON.parse(sessionStorage.getItem("user"));
    myMoney.text("Money: "+sessionStorage.getItem(user['username']));
}