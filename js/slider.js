document.addEventListener("DOMContentLoaded", function(event) {
    display(1);
    move();
});

var act = 1;

function display(n) {

    var x = document.getElementsByClassName("slider");

    if (n > x.length) act = 1;
    if (n < 1) act = x.length;

    for (var i = 0; i < x.length; i++) {
        x[i].style.display = "none";
    }
    x[act - 1].style = null;
    move();
}

function slid(n) {
    // console.log(act+" + "+n);
    display(act += n);

}


var progress = 0;
function move() {
    if (progress == 0) {
        progress = 1;
        var elem = document.getElementById("bar");
        var width = 1;
        var id = setInterval(frame, 15);
        function frame() {
            if (width >= 100) {
                clearInterval(id);
                progress = 0;
                slid(1);
            } else {
                width+=0.1;
                elem.style.width = width + "%";
            }
        }
    }
}