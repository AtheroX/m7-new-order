function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        
        reader.onload = function (e) {
            $('#imgUser').attr('src', e.target.result);
            sessionStorage.setItem('img', e.target.result);
        }
        
        reader.readAsDataURL(input.files[0]);
    }
}

function restaurarC() {
    
    var colorrgba = "rgba(44, 44, 44, 0.5)"; 
    sessionStorage.setItem("color",colorrgba);
    
    var abobole = document.getElementsByClassName("grid-container");
    for (let index = 0; index < abobole.length; index++) {            
        abobole[index].style.backgroundColor = colorrgba;            
    }
}

var colorsito = '#ffffff';
var alpha = .5;

function colorines() {
    colorsito = document.getElementById("btnColor").value.substring(1);
    
    var colorrgba = 'rgba('+parseInt(colorsito.substring(0,2),16)+","+parseInt(colorsito.substring(2,4),16)+","+parseInt(colorsito.substring(4,6),16)+","+alpha+')';  
    console.log(colorrgba);

    sessionStorage.setItem("color",colorrgba);

    var abobole = document.getElementsByClassName("grid-container");        
    for (let index = 0; index < abobole.length; index++) {            
        abobole[index].style.backgroundColor = colorrgba;

    }
}

function sliderCoso(value){
    alpha = value/100;

    var colorrgba = 'rgba('+parseInt(colorsito.substring(0,2),16)+","+parseInt(colorsito.substring(2,4),16)+","+parseInt(colorsito.substring(4,6),16)+","+alpha+')';  
    console.log(colorrgba);    
    sessionStorage.setItem("color",colorrgba);

    var abobole = document.getElementsByClassName("grid-container");
            
    for (let index = 0; index < abobole.length; index++) {            
        abobole[index].style.backgroundColor = colorrgba;
                    
    }
}


function restaurarC2() {
    
    var colorrgba = "rgba(44, 44, 44, 0.5)"; 
    sessionStorage.setItem("color2",colorrgba);
    
    var abobole = document.getElementsByName("coso");
    for (let index = 0; index < abobole.length; index++) {            
        abobole[index].style.backgroundColor = colorrgba;            
    }
}

var colorsito2 = '#ffffff';
var alpha2 = 1;

function colorines2() {
    colorsito2 = document.getElementById("btnColor2").value.substring(1);
    
    var colorrgba = 'rgba('+parseInt(colorsito2.substring(0,2),16)+","+parseInt(colorsito2.substring(2,4),16)+","+parseInt(colorsito2.substring(4,6),16)+","+alpha2+')';  
    console.log(colorrgba);

    sessionStorage.setItem("color2",colorrgba);

    var abobole = document.getElementsByName("coso");        
    for (let index = 0; index < abobole.length; index++) {            
        abobole[index].style.backgroundColor = colorrgba;

    }
}

function sliderCoso2(value){
    alpha2 = value/100;

    var colorrgba = 'rgba('+parseInt(colorsito2.substring(0,2),16)+","+parseInt(colorsito2.substring(2,4),16)+","+parseInt(colorsito2.substring(4,6),16)+","+alpha2+')';  
    console.log(colorrgba);    
    sessionStorage.setItem("color2",colorrgba);

    var abobole = document.getElementsByName("coso");
            
    for (let index = 0; index < abobole.length; index++) {            
        abobole[index].style.backgroundColor = colorrgba;
                    
    }
}



$(document).ready(function () {

    if(sessionStorage.getItem("color") != null && sessionStorage.getItem("color") != "null"){
        $('.grid-container').css("background-color", sessionStorage.getItem('color'));
    }
    

    if(sessionStorage.getItem('img')!=null){
        var reader = sessionStorage.getItem('img');
        console.log(reader);
        $('#imgUser').attr('src', reader);
        
    }
    $("#iconButton").change(function(){
        // $("#iconPrev").css({"display":"block"});
        console.log('cambiao');
        readURL(this);
    });

});