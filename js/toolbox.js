function logout(index = false){
    sessionStorage.setItem("user", null);
    if(index)
        window.location.href = "index.html";
    else
        window.location.href = "../index.html";
}

$(document).ready(function () {
    if(sessionStorage.getItem("user") == "null" || sessionStorage.getItem("user") == null)
        window.location.href = "../html/login.html";
    else{

    }
    var a = JSON.parse(sessionStorage.getItem('user'));
    if(a['admin']){
        $('#admin').text("ADMIN");
    }
    $('#perfilNav').text(a['username']);

    $('#username').text(a['username']);

    if(sessionStorage.getItem("color2") != null && sessionStorage.getItem("color2") != "null"){
        $('header').css("background-color", sessionStorage.getItem('color2'));
        $('footer').css("background-color", sessionStorage.getItem('color2'));
    }

});

function admin(){
    window.location.href = "admin.html";

}
