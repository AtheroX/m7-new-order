$(document).ready(function () {
    

    if(sessionStorage.getItem('webticker') == null || sessionStorage.getItem('webticker') == 'null'){
        var eventosPasados = [
            "La ciudad Nahant fue atacada por un grupo de radicales de Apendi City",
            "New Order ha instaurado un nuevo método de compra intercambiando órganos",
            "La nueva \"Utopia\" instaurada por New Order se llamará Salem y empieza con un capital de 37 millones recaudados por las diferentes ciudades"
        ];
        sessionStorage.setItem('webticker', eventosPasados.join(";"))
    }

    var eventos = sessionStorage.getItem('webticker').split(";");
    var i = 1;
    eventos.forEach(e => {
        // var a = $("<li></li>").text(e);
        var webtika = document.getElementById("webticker-update-example");

        webtika.innerHTML += '<li data-update="item'+i+'">'+e+'</li>';

        // console.log(webtika.innerHTML);
        i++;
    });

    $("#webticker-update-example").webTicker({
        duplicate:true, 
        // hoverpause:false,
        startEmpty:false,
        maskleft: '20px'

    });
});